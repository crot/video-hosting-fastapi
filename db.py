import ormar
import databases
import sqlalchemy


metadata = sqlalchemy.MetaData()
database = databases.Database("sqlite:///video_hosting.db")
engine = sqlalchemy.create_engine("sqlite:///video_hosting.db")


class MainMeta(ormar.ModelMeta):
    # tablename = "videos"
    metadata = metadata
    database = database
