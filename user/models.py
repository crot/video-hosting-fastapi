import ormar
from fastapi_users.db import OrmarBaseUserModel, OrmarUserDatabase

from .schemas import UserDB
from db import MainMeta


class User(OrmarBaseUserModel):
    """Таблица для Пользователя"""
    class Meta(MainMeta):
        pass

    username: str = ormar.String(max_length=100, unique=True)
    phone: str = ormar.String(max_length=14, unique=True, nullable=True)


user_db = OrmarUserDatabase(UserDB, User)
