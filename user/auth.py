from fastapi_users.authentication import JWTAuthentication
import fastapi_users

from .models import user_db
from .schemas import UserDB, UserUpdate, UserCreate, User

SECRET = "IKJ(*H(**{JDFAKJSDJA*UH*#(RU#IOJ:>L>{L:KLJNJUHUHN&**&^&*(^)(U"

auth_backends = []

jwt_authentication = JWTAuthentication(secret=SECRET, lifetime_seconds=3600)

auth_backends.append(jwt_authentication)

fastapi_users = fastapi_users.FastAPIUsers(
    user_db,
    auth_backends,
    User,
    UserCreate,
    UserUpdate,
    UserDB,
)

current_active_user = fastapi_users.current_user(active=True)
