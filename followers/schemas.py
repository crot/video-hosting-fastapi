import uuid

from pydantic import BaseModel

from user.schemas import UserOut, User


class FollowerCreate(BaseModel):
    """ Создание подписчика
    """
    username: str


class FollowerList(BaseModel):
    """ Список подписчиков
    """
    user: UserOut
    subscriber: UserOut
