from typing import List

from fastapi import (
    UploadFile,
    File,
    APIRouter,
    Form,
    BackgroundTasks,
    Request, Depends,
)
from starlette.templating import Jinja2Templates
from starlette.responses import StreamingResponse, HTMLResponse

from user.auth import current_active_user
from .schemas import (
    GetVideo,
    Message,
    GetListVideo
)
from .models import Video, User
from .services import save_video, open_file


video_router = APIRouter(tags=["video"])
templates = Jinja2Templates(directory='templates')


@video_router.post('/')
async def create_video(
        # background_tasks: BackgroundTasks,
        title: str = Form(...),
        description: str = Form(...),
        file: UploadFile = File(...),
        user: User = Depends(current_active_user)
):
    """ Загружает видео
    """
    return await save_video(
        user=user,
        file=file,
        title=title,
        description=description,
        # background_tasks=background_tasks
    )


# @video_route.get(
#     '/video/{video_pk}',
#     response_model=GetVideo,
#     responses={404: {'model': Message}}
# )
# async def get_video(video_pk: int):
#     """Возвращает конкретное видео"""
#     file = await Video.objects.select_related('user').get(pk=video_pk)
#     file_like = open(file.file, mode='rb')
#     return StreamingResponse(file_like, media_type='video/mp4')


# @video_route.get(
#     '/video/{video_pk}',
#     response_class=HTMLResponse
# )
# async def get_video(request: Request, video_pk: int):
#     """Возвращает конкретное видео (рендеринг в html)"""
#     return templates.TemplateResponse(
#         'index.html',
#         {"request": request, "path": video_pk}
#     )


@video_router.get("/video/{video_pk}")
async def get_streaming_video(
        request: Request,
        video_pk: int
) -> StreamingResponse:
    file, status_code, content_length, headers = await open_file(
        request, video_pk
    )
    response = StreamingResponse(
        file,
        media_type='video/mp4',
        status_code=status_code,
    )

    response.headers.update({
        'Accept-Ranges': 'bytes',
        'Content-Length': str(content_length),
        **headers,
    })
    return response


@video_router.get(
    '/user-video/{user_pk}',
    response_model=List[GetListVideo],
)
async def get_list_video(user_pk: str):
    """ Возвращает список видео
    """
    video_list = await Video.objects.filter(user=user_pk).all()
    return video_list


@video_router.post("/{video_pk}", status_code=201)
async def add_like(video_pk: int, user: User = Depends(current_active_user)):
    """ Добавляет лайк к видео
    """
    _video = await Video.objects.select_related("like_user").get(pk=video_pk)
    _user = await User.objects.get(id=user.id)
    if _user in _video.like_user:
        _video.like_count -= 1
        await _video.like_user.remove(_user)
    else:
        _video.like_count += 1
        await _video.like_user.add(_user)
    await _video.update()
    return _video.like_count
