from pydantic import BaseModel

from user.schemas import UserDB


class Message(BaseModel):
    message: str


class UploadVideo(BaseModel):
    title: str
    description: str


class GetListVideo(BaseModel):
    id: int
    title: str
    description: str


class GetVideo(GetListVideo):
    user: UserDB
