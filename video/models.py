import datetime
from typing import Optional, Dict, Union, List

import ormar

from user.models import User
from db import MainMeta


class UserLike(ormar.Model):
    """ Таблица для лайков на видео
    """
    class Meta(MainMeta):
        pass

    id: int = ormar.Integer(primary_key=True)


class Video(ormar.Model):
    """ Таблица для Видео
    """
    class Meta(MainMeta):
        pass

    id: int = ormar.Integer(primary_key=True)
    title: str = ormar.String(max_length=50)
    description: str = ormar.String(max_length=500)
    file: str = ormar.String(max_length=1000)
    create_at: datetime.datetime = ormar.DateTime(
        default=datetime.datetime.now
    )
    user: Optional[Union[User, Dict]] = ormar.ForeignKey(User)
    like_count: int = ormar.Integer(default=0)
    like_user: Optional[Union[List[User], Dict]] = ormar.ManyToMany(
        User, related_name="like_users"
    )
